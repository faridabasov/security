package az.ingress.security.controller;

import az.ingress.security.dto.JwtDto;
import az.ingress.security.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;

@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
public class JwtController {
    private final JwtService jwtService;

    @PostMapping("/sign-in")
    public JwtDto signIn(Authentication authentication){
        String token  = jwtService.issueToken(authentication, Duration.ofMinutes(30));
        return new JwtDto(token);
    }
}
