package az.ingress.security.controller;

import az.ingress.security.adapter.BookAdapter;
import az.ingress.security.dto.BookDto;
import az.ingress.security.entity.Book;
import az.ingress.security.service.BookService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/api")
public class BookController {
    private final BookService bookService;
    private final BookAdapter adapter;

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/book/{id}")
    public Book getBook(@PathVariable Long id) {
        return bookService.getBook(id);
    }

    @GetMapping("/books/{author}")
    public List<Book> getBookByAuthor(@PathVariable String author) {
        return bookService.getBooksByAuthor(author);
    }

    @PostMapping("/book")
    public void addNewBook(@RequestBody BookDto dto){
        bookService.saveBook(adapter.map(dto));
    }

    @PutMapping("/book/{id}")
    public void updateBook(@RequestBody BookDto dto, @PathVariable Long id){
        bookService.updateBook(adapter.map(dto), id);
    }
}
