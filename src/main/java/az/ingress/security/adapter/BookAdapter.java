package az.ingress.security.adapter;

import az.ingress.security.dto.BookDto;
import az.ingress.security.entity.Book;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookAdapter {
    Book map(BookDto dto);
}
