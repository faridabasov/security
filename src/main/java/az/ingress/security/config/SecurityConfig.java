package az.ingress.security.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilterConfig jwtFilterConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/v1/api/books/**").hasAnyAuthority("USER")
                .antMatchers(HttpMethod.POST,"/v1/api/book").hasAnyAuthority("PUBLISHER")
                .antMatchers(HttpMethod.PUT, "/v1/api/book/**").hasAnyAuthority("PUBLISHER")
                .antMatchers("/v1/api/**").hasAnyAuthority("USER", "PUBLISHER")
                .anyRequest().authenticated()
                .and()
                .httpBasic();
        log.info("SecurityConfig start");

        http.apply(jwtFilterConfig);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
