package az.ingress.security;

import az.ingress.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class SecurityApplication {

	private final UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		Authority userAuthority = Authority
//				.builder()
//				.authority("USER")
//				.build();
//
//		Authority publisherAuthority = Authority
//				.builder()
//				.authority("PUBLISHER")
//				.build();
//
//		User user = User
//				.builder()
//				.password("fazil") // TODO
//				.username("user")
//				.isAccountNonExpired(true)
//				.isAccountNonLocked(true)
//				.isCredentialsNonExpired(true)
//				.isEnabled(true)
//				.authorities(Set.of(userAuthority))
//				.build();
//
//		User publisher = User
//				.builder()
//				.password("fazil") // TODO
//				.username("publisher")
//				.isAccountNonExpired(true)
//				.isAccountNonLocked(true)
//				.isCredentialsNonExpired(true)
//				.isEnabled(true)
//				.authorities(Set.of(userAuthority, publisherAuthority))
//				.build();
//
//		userRepository.save(publisher);
//		userRepository.save(user);
//	}
}
