package az.ingress.security.jwt;

import az.ingress.security.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class JwtService {
    private Key key;

    @Autowired
    private UserRepository userRepository;

    public JwtService() {
        byte[] byteKey;
        byteKey = Decoders.BASE64.decode("ZmF6aWwgbWF6aWwgc2Rua2ogayBrd2VuZiBrbmYgIHdlaHV3ZWhmdXdlZmllaGZ1ZWZqZSBqd2VmaHdlZmt3IGtqZm5la2JmZWJmIGtqYmZrZWJuZmtqd25lZCBrd2puZGtqd2ViZml3amUga2p3ZW5qd2VuZmtqd2VmIGt3amVmbmtqd2ViZmtqd2Uga3dqYmZrd2ViZmsga2p3ZW5mb2V3YmY=");
        key = Keys.hmacShaKeyFor(byteKey);
    }

    public String issueToken(Authentication authentication, Duration duration) {
        JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("roles", getRoles(authentication));

        return jwtBuilder.compact();
    }

    private Set<String> getRoles(Authentication authentication) {
       return authentication
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public static void main(String[] args) {
        GrantedAuthority user = new SimpleGrantedAuthority("USER");
        GrantedAuthority publisher = new SimpleGrantedAuthority("PUBLISHER");
        Authentication authentication = new UsernamePasswordAuthenticationToken("farid", "", Set.of(user, publisher));
        JwtService jwtService = new JwtService();
        String token = jwtService.issueToken(authentication, Duration.ofSeconds(1800));
//        System.out.println(token);
        Claims claims = jwtService.parseToken(token);
        System.out.println("---------------------");
        System.out.println(claims.toString());
    }
}
