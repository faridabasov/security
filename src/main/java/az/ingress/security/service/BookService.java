package az.ingress.security.service;

import az.ingress.security.entity.Book;
import az.ingress.security.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;
    public List<Book> getAllBooks(){
        return bookRepository.findAll();
    }

    public Book getBook(Long id){
        return bookRepository
                .findById(id)
                .orElseThrow(()->new RuntimeException("Book Not Found"));
    }

    public List<Book> getBooksByAuthor(String author){
        return bookRepository.findByAuthor(author);
    }

    public void saveBook(Book book){
        bookRepository.save(book);
    }

    @Transactional
    public void updateBook(Book book, Long id){
        Book oldBook = bookRepository
                .findById(id)
                .orElseThrow(()->new RuntimeException("Book Not Found"));
       oldBook.setName(book.getName());
       oldBook.setAuthor(book.getAuthor());
    }
}
